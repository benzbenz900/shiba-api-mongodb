const bodyParser = require('body-parser')
const express = require('express');
const app = express();
const connect = { useUnifiedTopology: true }
const MongoClient = require('mongodb').MongoClient;
const compression = require('compression')
var cors = require('cors')
var ObjectId = require('mongodb').ObjectID;
require('dotenv').config();
const cacheControl = require('express-cache-controller');
var apicache = require('apicache')

const url = process.env.DATA_BASE || "mongodb+srv://babauser:babauser@cluster0.6mvje.gcp.mongodb.net/?retryWrites=true&w=majority";
const databaseName = process.env.DATA_BASE_NAME || "ShibaBookShop";
const AutoIncrement = false;
const CorsOrigin = '*' // https://domain.com

class App {
    constructor() {
        this.query = {}
        this.limitFind = 0
        this.cache = apicache.middleware
        var corsOptions = {
            origin: CorsOrigin,
            optionsSuccessStatus: 200
        }
        app.use(cors(corsOptions))
        app.use(cacheControl({ maxAge: 60 }));
        app.use(bodyParser.urlencoded({ extended: false }))
        app.use(bodyParser.json({
            inflate: true,
            limit: '1024kb',
            type: 'application/json'
        }))
        app.use(compression({
            level: 9
        }))
    }

    run() {

        app.get(['/', '/index', '/index.html'], this.cache('48 hour'), (req, res) => {
            res.setHeader('Content-Type', 'text/html');
            res.send('API Work!!');
        });

        app.get(['/api/:collection', '/api/:collection/:id'], (req, res) => {
            res.setHeader('Content-Type', 'application/json');
            this.ActionGet(req, res)
        });

        app.post(['/api/:collection', '/api/:collection/:id'], (req, res) => {
            res.setHeader('Content-Type', 'application/json');
            this.ActionPost(req, res)
        });

        app.delete(['/api/:collection', '/api/:collection/:id'], (req, res) => {
            res.setHeader('Content-Type', 'application/json');
            this.ActionDelete(req, res)
        });

        app.put(['/api/:collection'], (req, res) => {
            res.setHeader('Content-Type', 'application/json');
            this.ActionPut(req, res)
        });

        app.listen(process.env.PORT || 3000, () => {
            console.log(`Example app listening on ${process.env.PORT || 3000} port!`);
        });
    }

    ActionGet = (req, res) => {
        if (req.query.q) {
            this.query = JSON.parse(req.query.q)
            if (this.query._id) {
                this.query._id = new ObjectId(this.query._id)
            }
        } else {
            if (req.params.id) {
                if (AutoIncrement) {
                    this.query._id = parseInt(req.params.id)
                } else {
                    this.query._id = new ObjectId(req.params.id)
                }
            }
        }
        this.project = {}
        if (req.query.project) {
            var project = req.query.project.split(",");
            this.project = '';
            project.forEach((element, i) => {
                if (i == project.length - 1) {
                    this.project += `"${element.trim()}": 1`
                } else {
                    this.project += `"${element.trim()}": 1,`
                }

            });
            this.project = JSON.parse(`{${this.project}}`)
        }

        this.sort = {}
        if (req.query.sort) {
            if (req.query.sort == 'asc') {
                this.sort = { "_id": 1 }
            } else {
                this.sort = { "_id": -1 }
            }
        }

        this.skips = 0
        if (req.query.skip) {
            this.skips = parseInt(req.query.skip)
        }

        if (req.query.limit) {
            this.limitFind = parseInt(req.query.limit)
        }

        const collectionDB = req.params.collection;
        MongoClient.connect(url, connect, (err, db) => {
            const dbo = db.db(databaseName);
            if (this.limitFind == 1) {
                dbo.collection(collectionDB)
                    .find(this.query)
                    .limit(1)
                    .project(this.project)
                    .toArray((err, result) => {
                        if (err) console.log(err);
                        if (result.length != 0) {
                            dbo.collection(collectionDB)
                                .find(this.query).count().then(total => {
                                    result[0].q_item_total = total
                                    res.json(result);
                                    db.close();
                                })
                        } else {
                            res.json(result);
                            db.close();
                        }
                        this.limitFind = 0
                        this.query = {}
                    })
            } else if (this.limitFind == 0) {
                dbo.collection(collectionDB)
                    .find(this.query)
                    .sort(this.sort)
                    .project(this.project)
                    .toArray((err, result) => {
                        if (err) console.log(err);
                        if (result.length != 0) {
                            dbo.collection(collectionDB)
                                .find(this.query).count().then(total => {
                                    result[0].q_item_total = total
                                    res.json(result);
                                    db.close();
                                })
                        } else {
                            res.json(result);
                            db.close();
                        }
                        this.limitFind = 0
                        this.query = {}
                    })
            } else {
                dbo.collection(collectionDB)
                    .find(this.query)
                    .sort(this.sort)
                    .skip(this.skips)
                    .limit(this.limitFind)
                    .project(this.project)
                    .toArray((err, result) => {
                        if (err) console.log(err);
                        if (result.length != 0) {
                            dbo.collection(collectionDB)
                                .find(this.query).count().then(total => {
                                    result[0].q_item_total = total
                                    res.json(result);
                                    db.close();
                                })
                        } else {
                            res.json(result);
                            db.close();
                        }
                        this.limitFind = 0
                        this.query = {}
                    })
            }
        });
    }
    ActionPost = (req, res) => {

        if (req.is() == 'application/json' && req.body) {
            this.query = req.body
        }

        if (req.is() == 'application/x-www-form-urlencoded' && req.body) {
            this.query = req.body
        }

        if (req.query.limit) {
            this.limitFind = parseInt(req.query.limit)
        }

        if (req.query.q) {
            this.filter = JSON.parse(req.query.q)
        } else {
            if (AutoIncrement) {
                this.filter = { '_id': parseInt(req.params.id) }
            } else {
                this.filter = { '_id': new ObjectId(req.params.id) }
            }
        }

        const collectionDB = req.params.collection;
        MongoClient.connect(url, connect, (err, db) => {
            const dbo = db.db(databaseName);
            dbo.collection(collectionDB).updateMany(this.filter, { $set: this.query }, (err, result) => {
                if (err) console.log(err);
                res.json({ 'ok': result.result.ok });
                db.close();
            });
        });
    }
    ActionDelete = (req, res) => {

        if (req.is() == 'application/json' && req.body) {
            this.query = req.body
        }

        if (req.is() == 'application/x-www-form-urlencoded' && req.body) {
            this.query = req.body
        }

        if (req.query.limit) {
            this.limitFind = parseInt(req.query.limit)
        }

        if (req.query.q) {
            this.filter = JSON.parse(req.query.q)
        } else {
            if (AutoIncrement) {
                this.filter = { '_id': parseInt(req.params.id) }
            } else {
                this.filter = { '_id': new ObjectId(req.params.id) }
            }
        }

        const collectionDB = req.params.collection;
        MongoClient.connect(url, connect, (err, db) => {
            const dbo = db.db(databaseName);
            dbo.collection(collectionDB).deleteMany(this.filter, { $set: this.query }, (err, result) => {
                if (err) console.log(err);
                res.json({ 'ok': result.result.ok });
                db.close();
            });
        });
    }
    ActionPut = (req, res) => {

        if (req.is() == 'application/json' && req.body) {
            this.query = req.body
        }

        if (req.is() == 'application/x-www-form-urlencoded' && req.body) {
            this.query = req.body
        }

        if (req.query.limit) {
            this.limitFind = parseInt(req.query.limit)
        }

        const collectionDB = req.params.collection;
        MongoClient.connect(url, connect, async (err, db) => {

            const dbo = db.db(databaseName);

            if (AutoIncrement) {
                this.getNextId(collectionDB, dbo).then(id => {
                    this.query._id = id
                }).then(() => {
                    dbo.collection(collectionDB).insertOne(this.query, (err, result) => {
                        if (err) console.log(err);
                        res.send(result.ops);
                    });
                })
            } else {
                dbo.collection(collectionDB).insertOne(this.query, (err, result) => {
                    if (err) console.log(err);
                    res.send(result.ops);
                });
            }
        });
    }

    getNextId(collection, dbo) {
        return new Promise(rest => {
            dbo.collection('counters').findOneAndUpdate(
                { _id: `${collection}_id` },
                { $inc: { lastId: 1 } },
                { upsert: true },
                (err, res) => {
                    if (res.value === null) {
                        rest(1);
                    } else {
                        rest(res.value.lastId);
                    }
                }
            );
        })
    }


}

new App().run()