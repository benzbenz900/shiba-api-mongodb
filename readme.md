Shiba Book Shop MongoDB
=====================================

### ติดตั้ง
```bash
$ npm install
$ node api.js
```

**http://localhost:3000/**

## Docker Setup

```bash
# docker build image
$ docker build -t shiba-api .

# serve with hot reload at localhost:3000
$ docker run -e DATA_BASE="mongodb+srv://babauser:babauser@cluster0.6mvje.gcp.mongodb.net/?retryWrites=true&w=majority" -e DATA_BASE_NAME="ShibaBookShop" -p 3000:3000 -d shiba-api
```

## Docker Compose docker-compose.yml

```bash
version: "3.3"
services:
    shiba:
        environment:
            - DATA_BASE="mongodb+srv://babauser:babauser@cluster0.6mvje.gcp.mongodb.net/?retryWrites=true&w=majority"
            - DATA_BASE_NAME="ShibaBookShop"
        ports:
            - "3000:3000"
        image: shiba-api

```

## Docker Compose Up

```bash
$ docker-compose up -d
```

## Deploy on Heroku
```bash
$ heroku login
$ heroku git:remote -a shiba-api #your git heruku name https://git.heroku.com/shiba-api.git
$ heroku config:set DATA_BASE="mongodb+srv://babauser:babauser@cluster0.6mvje.gcp.mongodb.net/?retryWrites=true&w=majority"
$ heroku config:set DATA_BASE_NAME="ShibaBookShop"
$ git add .
$ git commit -am "make it better"
$ git push heroku master
```

### ตัวแปร

**:collection** collection name หรือ table หากเรียกตาม Mysql

**?limit** Limit จำนวนที่แสดงออกมา **0=ทั้งหมด หรือไม่ใส่**

**?q** รูปแบบเป็น JSON ใช้ filter ข้อมูล เช่น
`{"name":"car","price":1500000}`{.language-json}

### รูปแบบ URL เรียกใช้งาน Method GET POST PUT DELETE

#### Method PUT เพิ่มข้อมูล

เพิ่มข้อมูลผ่าน ได้ 2 แบบ คือ **application/json** และ
**application/x-www-form-urlencoded**

**PUT** http://localhost:3000/api/:collection เพิ่มข้อมูลใหม่

#### Method GET ดึงข้อมูล

**GET** http://localhost:3000/api/:collection ดึงข้อมูล ?q={json} และ
?limit=number
?skip=number
**GET** http://localhost:3000/api/:collection/:id ดึงข้อมูล
?limit=number
?skip=number
#### Method POST แก้ไขข้อมูล

**POST** http://localhost:3000/api/:collection แก้ไขข้อมูล ?q={json}

**POST** http://localhost:3000/api/:collection/:id แก้ไขข้อมูล

#### Method DELETE ลบข้อมูล

**DELETE** http://localhost:3000/api/:collection ลบข้อมูล ?q={json}

**DELETE** http://localhost:3000/api/:collection/:id ลบข้อมูล